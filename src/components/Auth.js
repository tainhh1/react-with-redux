import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { actions as userActions } from '../store/slices/userProfileSlice';
import classes from './Auth.module.css';
import UserProfile from './UserProfile';

const isNotEmpty = (value) => value.trim() !== '';

const validateEmail = (email) => {
	const re =
		/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(email);
};

const Auth = () => {
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const isEmailValid = isNotEmpty(email) && validateEmail(email);
	const isPasswordValid = isNotEmpty(password);
	const isLoggedIn = useSelector((state) => state.userReducer.isLoggedIn);
	const dispatch = useDispatch();

	const emailChangeHandler = (event) => {
		setEmail(event.target.value);
	};

	const passwordChangeHandler = (event) => {
		setPassword(event.target.value);
	};

	let isFormValid = false;
	if (isEmailValid && isPasswordValid) {
		isFormValid = true;
	}

	const submitAuthFormHandler = (event) => {
		event.preventDefault();
		if (!isFormValid) {
			return;
		}
		dispatch(userActions.login());
		dispatch(userActions.getEmail({ email }));
		setEmail('');
		setPassword('');
	};

	const formContext = (
		<section>
			<form onSubmit={submitAuthFormHandler}>
				<div className={classes.control}>
					<label htmlFor='email'>Email</label>
					<input
						value={email}
						onChange={emailChangeHandler}
						type='email'
						id='email'
					/>
					{!isFormValid && (
						<p className={classes['error-text']}>Email is not valid</p>
					)}
				</div>
				<div className={classes.control}>
					<label htmlFor='password'>Password</label>
					<input
						value={password}
						onChange={passwordChangeHandler}
						type='password'
						id='password'
					/>
					{!isFormValid && (
						<p className={classes['error-text']}>Password is not valid</p>
					)}
				</div>
				<button>Login</button>
			</form>
		</section>
	);
	const formClass = `${isLoggedIn ? '' : classes.auth}`;

	return (
		<main className={formClass}>
			{isLoggedIn ? <UserProfile /> : formContext}
		</main>
	);
};

export default Auth;
