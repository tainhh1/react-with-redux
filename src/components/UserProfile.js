import { useSelector } from 'react-redux';
import classes from './UserProfile.module.css';

const UserProfile = () => {
	const userProfile = useSelector((state) => state.userReducer.email);
	return (
		<main className={classes.profile}>
			<h2>{userProfile}</h2>
		</main>
	);
};

export default UserProfile;
