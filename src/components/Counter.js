import classes from './Counter.module.css';
import { useSelector, useDispatch } from 'react-redux';
import { actions as counterActions } from '../store/slices/counterSlice';

const Counter = () => {
	const counter = useSelector((state) => state.counterReducer.counter);
	const isShow = useSelector((state) => state.counterReducer.showCounter);
	const dispatch = useDispatch();

	const toggleCounterHandler = () => {
		dispatch(counterActions.toggleCounter());
	};
	const decrementHandler = () => {
		dispatch(counterActions.decrement());
	};
	const inCreaseByFiveHandler = () => {
		dispatch(counterActions.increase5(5));
	};
	const incrementHandler = () => {
		dispatch(counterActions.increment());
	};

	return (
		<main className={classes.counter}>
			<h1>Redux Counter</h1>
			{isShow && <div className={classes.value}>{counter}</div>}
			<button onClick={toggleCounterHandler}>Toggle Counter</button>
			<div>
				<button onClick={decrementHandler}>-</button>
				<button onClick={inCreaseByFiveHandler}>Increase By 5</button>
				<button onClick={incrementHandler}>+</button>
			</div>
		</main>
	);
};

export default Counter;

// class Counter extends Component {
// 	constructor() {
// 		super();
// 		this.state = { isShowCounter: true };
// 	}

// 	toggleCounterHandler() {
// 		const prevState = this.state.isShowCounter;
// 		this.setState({ isShowCounter: !prevState });
// 	}

// 	incrementHandler() {
// 		this.props.increment();
// 	}

// 	decrementHandler() {
// 		this.props.decrement();
// 	}
// 	render() {
// 		return (
// 			<main className={classes.counter}>
// 				<h1>Redux Counter</h1>
// 				{this.state.isShowCounter && (
// 					<div className={classes.value}>{this.props.counter}</div>
// 				)}
// 				<button onClick={this.toggleCounterHandler.bind(this)}>
// 					Toggle Counter
// 				</button>
// 				<div>
// 					<button onClick={this.decrementHandler.bind(this)}>-</button>
// 					<button onClick={this.incrementHandler.bind(this)}>+</button>
// 				</div>
// 			</main>
// 		);
// 	}
// }

// const mapStateToProps = (state) => {
// 	return { counter: state.counter };
// };

// const mapDispatchToProps = (dispatch) => {
// 	return {
// 		increment: () => dispatch({ type: 'increment' }),
// 		decrement: () => dispatch({ type: 'decrement' }),
// 	};
// };

// export default connect(mapStateToProps, mapDispatchToProps)(Counter);
