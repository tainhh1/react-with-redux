import { useSelector, useDispatch } from 'react-redux';
import { actions as userActions } from '../store/slices/userProfileSlice';
import classes from './Header.module.css';

const Header = () => {
	const isLoggedIn = useSelector((state) => state.userReducer.isLoggedIn);
	const dispatch = useDispatch();

	const logoutHandler = () => {
		dispatch(userActions.logout());
	};

	const headerContext = (
		<nav>
			<ul>
				<li>
					<a href='/'>My Products</a>
				</li>
				<li>
					<a href='/'>My Sales</a>
				</li>
				<li>
					<button onClick={logoutHandler}>Logout</button>
				</li>
			</ul>
		</nav>
	);
	return (
		<header className={classes.header}>
			<h1>Redux Auth</h1>
			{isLoggedIn && headerContext}
		</header>
	);
};

export default Header;
