import { configureStore } from '@reduxjs/toolkit';
import { reducer as counterReducer } from './slices/counterSlice';
import { reducer as userReducer } from './slices/userProfileSlice';

const store = configureStore({
	reducer: { counterReducer, userReducer },
});

export default store;
