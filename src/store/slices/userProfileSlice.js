import { createSlice } from '@reduxjs/toolkit';

const initialState = { email: '', isLoggedIn: false };

const userSlice = createSlice({
	name: 'user',
	initialState,
	reducers: {
		getEmail(state, action) {
			state.email = action.payload.email;
		},
		login(state) {
			state.isLoggedIn = true;
		},
		logout(state) {
			state.isLoggedIn = false;
		},
	},
});

export const { reducer, actions } = userSlice;
